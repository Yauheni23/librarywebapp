package com.company;

import java.util.Date;

/**
 * Created by Evgeny on 06.07.2017.
 */
public class Book{

    private String bookName;
    private Date datePublish;
    private String author;
    private String publisherName;
    
    Book(String name, Date date, String author, String publisher){
        this.bookName = name;
        this.datePublish = date;
        this.author = author;
        this.publisherName = publisher;
    }


    public void setName(String name){
        this.bookName = name;
    }
    public String getName(){
        return this.bookName;
    }
    public void setDate(Date date){
        this.datePublish = date;
    }
    public Date getDate(){
        return this.datePublish;
    }
    public void setAuthor(String author){
        this.author = author;
    }
    public String getAuthor(){
        return this.author;
    }
    public void setPublisher(String publisher){
        this.publisherName = publisher;
    }
    public String getPublisher(){
        return this.publisherName;
    }

    @Override
    public String toString() {
        return this.bookName+", "+this.datePublish+", by "+this.author + ", " + this.publisherName + "\n";
    }
}

