package com.company;

import java.io.IOException;
import java.io.PrintWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Evgeny on 07.07.2017.
 */
public class HomeServlet extends javax.servlet.http.HttpServlet {

    private static final Logger LOGGER = LoggerFactory.getLogger(HomeServlet.class);
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<body>");
        out.println("<h1>Hello Servlet Get</h1>");
        BookService bookService = new BookService();
        LOGGER.info("Start method for books");
        for (Book book : bookService.createBooksList()
             ) {
            out.println("<h1>"+book.toString()+"</h1>");
            LOGGER.debug("Book name is {}", book.getName());
        }
        LOGGER.info("End method");
        out.println("</body>");
        out.println("</html>");
    }
}
