
package com.company;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Evgeny on 07.07.2017.
 */
public class BookService {

    private List<Book> books;

    BookService(){
        books = new ArrayList<Book>();
    }


    public List<Book> createBooksList(){

        Book javaBook = new Book("JavaBook",new Date(1991,12,21),"Joe","Addison-Wesley");
        Book journalistBook = new Book("Journalist",new Date(2005,05,11),"Bob", "New-York-Publisher");
        Book programmingBook = new Book("Programming and a little bit more",new Date(2015,10,23),"Adam", "Piter");


        List<Book> booksList = new ArrayList<Book>();

        booksList.add(javaBook);
        booksList.add(journalistBook);
        booksList.add(programmingBook);

        return booksList;
    }
}
